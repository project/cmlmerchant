<?php

namespace Drupal\cmlmerchant\Hook;

/**
 * @file
 * Contains \Drupal\cmlmerchant\Hook\Cron.
 */

/**
 * Hook Cron.
 */
class Cron {

  /**
   * Hook.
   */
  public static function hook() {
    \Drupal::service('cmlmerchant.yml')->createFeeds();
  }

}

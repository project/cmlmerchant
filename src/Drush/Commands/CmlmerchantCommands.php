<?php

namespace Drupal\cmlmerchant\Drush\Commands;

use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class CmlmerchantCommands extends DrushCommands {

  /**
   * Create a CML feeds.
   */
  #[CLI\Command(name: 'cmlmerchant:feeds', aliases: ['feeds'])]
  #[CLI\Usage(name: 'cmlmerchant:feeds', description: 'Create a CML feeds')]
  public function feeds() {
    $this->output()->writeln("Создаем фиды");
    \Drupal::service('cmlmerchant.yml')->createFeeds();
  }

}

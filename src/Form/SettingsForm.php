<?php

namespace Drupal\cmlmerchant\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cmlmerchant.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->host = \Drupal::request()->getHost();
    $this->fields = \Drupal::service('cmlmerchant.yml')->getOtherFields();
    $this->config = $this->config('cmlmerchant.settings');
    $vk_yandex = $this->getDescription('vk_yandex', '/cmlmerchant/vk-yandex-feed.xml');
    $vk_google = $this->getDescription('vk_google', '/cmlmerchant/vk-google-feed.xml');
    $vk_realty = $this->getDescription('vk_realty', '/cmlmerchant/vk-realty-feed.xml');
    $vk_transport = $this->getDescription('vk_transport', '/cmlmerchant/vk-transport-feed.xml');
    $vk_services = $this->getDescription('vk_services', '/cmlmerchant/vk-services-feed.xml');
    $vk_hotel = $this->getDescription('vk_hotel', '/cmlmerchant/vk-hotel-feed.xml');
    $form['shop_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Название магазина'),
      '#maxlength' => 100,
      '#size' => 100,
      '#default_value' => $this->config->get('shop_name'),
    ];
    $form['company_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Название компании'),
      '#maxlength' => 150,
      '#size' => 150,
      '#default_value' => $this->config->get('company_name'),
    ];
    $form['site_url'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Пример: https://synapse-studio.ru'),
      '#title' => $this->t('Адрес сайта'),
      '#maxlength' => 150,
      '#size' => 150,
      '#default_value' => $this->config->get('site_url'),
    ];
    $form['vk'] = [
      '#type' => 'details',
      '#title' => 'Vk feed',
      '#open' => FALSE,
    ];
    $form['vk']["vk_yandex"] = [
      '#type' => 'checkbox',
      '#title' => "Фиды для товаров по спецификации Yandex",
      '#description' => $vk_yandex,
      '#default_value' => $this->config->get("vk_yandex"),
    ];
    $form['vk']["vk_google"] = [
      '#type' => 'checkbox',
      '#title' => "Фиды для товаров по спецификации Google",
      '#description' => $vk_google,
      '#default_value' => $this->config->get("vk_google"),
    ];
    $form['vk']["vk_realty"] = [
      '#type' => 'checkbox',
      '#title' => "Фиды для объектов недвижимости по спецификации Yandex",
      '#description' => $vk_realty,
      '#default_value' => $this->config->get("vk_realty"),
    ];
    $form['vk']["vk_transport"] = [
      '#type' => 'checkbox',
      '#title' => "Фиды для транспортных средств по спецификации Yandex",
      '#description' => $vk_transport,
      '#default_value' => $this->config->get("vk_transport"),
    ];
    $form['vk']["vk_services"] = [
      '#type' => 'checkbox',
      '#title' => "Фиды для сервисов услуг",
      '#description' => $vk_services,
      '#default_value' => $this->config->get("vk_services"),
    ];
    $form['vk']["vk_hotel"] = [
      '#type' => 'checkbox',
      '#title' => "Фиды для гостиниц",
      '#description' => $vk_hotel,
      '#default_value' => $this->config->get("vk_hotel"),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cmlmerchant.settings')
      ->set('shop_name', $form_state->getValue('shop_name'))
      ->set('company_name', $form_state->getValue('company_name'))
      ->set('site_url', $form_state->getValue('site_url'))
      ->set('vk_yandex', $form_state->getValue('vk_yandex'))
      ->set('vk_google', $form_state->getValue('vk_google'))
      ->set('vk_realty', $form_state->getValue('vk_realty'))
      ->set('vk_transport', $form_state->getValue('vk_transport'))
      ->set('vk_services', $form_state->getValue('vk_services'))
      ->set('vk_hotel', $form_state->getValue('vk_hotel'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  private function getDescription(string $feed, string $file) :array {
    $host = \Drupal::request()->getHost();
    $description = "<b>Фид:</b> <a href='$file' target='_blank'>{$this->host}$file</a><br>";
    $description .= $this->getFields($feed);
    return [
      '#markup' => $description,
    ];
  }

  /**
   * {@inheritdoc}
   */
  private function getFields(string $feed) :string {
    if (!$feed) {
      return '';
    }
    if (!isset($this->fields[$feed])) {
      return '';
    }
    if ($this->config->get($feed)) {
      $this->checkFields($feed);
    }
    $markup = '<b>Поля для фида:</b><br><ul>';
    foreach ($this->fields[$feed] as $field_name => $field_info) {
      $markup .= "<li>{$field_info['bundle']} - <b>$field_name</b> - {$field_info['type']}</li>";
    }
    $markup .= '</ul>';
    return $markup;
  }

  /**
   * {@inheritdoc}
   */
  private function checkFields(string $feed) {
    $product_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('commerce_product', 'product');
    foreach ($this->fields[$feed] as $field_name => $field_info) {
      if (!in_array($field_name, array_keys($product_fields))) {
        \Drupal::messenger()->addError("Поля {$field_name} в товаре не существует!");
      }
    }
  }

}

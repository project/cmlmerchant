<?php

namespace Drupal\cmlmerchant\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\cmlmerchant\Service\YmlServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;


/**
 * FeedController.
 */
class FeedController extends ControllerBase {

  const YANDEX_FILE = DRUPAL_ROOT . '/sites/default/files/YML/cmlmerchant_yandex.xml';
  const GOOGLE_FILE = DRUPAL_ROOT . '/sites/default/files/YML/cmlmerchant_google.xml';
  const VK_YANDEX_FILE = DRUPAL_ROOT . '/sites/default/files/YML/cmlmerchant_vk_yandex.xml';
  const VK_GOOGLE_FILE = DRUPAL_ROOT . '/sites/default/files/YML/cmlmerchant_vk_google.xml';
  const VK_REALTY_FILE = DRUPAL_ROOT . '/sites/default/files/YML/cmlmerchant_vk_realty.xml';
  const VK_TRANSPORT_FILE = DRUPAL_ROOT . '/sites/default/files/YML/cmlmerchant_vk_transport.xml';
  const VK_SERVICES_FILE = DRUPAL_ROOT . '/sites/default/files/YML/cmlmerchant_vk_services.xml';
  const VK_HOTEL_FILE = DRUPAL_ROOT . '/sites/default/files/YML/cmlmerchant_vk_hotel.xml';
  
  /**
   * Drupal\cmlmerchant\Service\YmlServiceInterface definition.
   *
   * @var \Drupal\cmlmerchant\Service\YmlServiceInterface
   */
  protected $yml;

  /**
   * Construct new FeedController.
   *
   * @param \Drupal\cmlmerchant\Service\YmlServiceInterface $ymlService
   *   YML feed generator service.
   */
  public function __construct(YmlServiceInterface $ymlService) {
    $this->yml = $ymlService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cmlmerchant.yml')
    );
  }

  /**
   * Страница тестирования формирования фида.
   */
  public function debug() {
    $this->yml->debug();
    return [];
  }

  /**
   * Страница yandex.xml
   */
  public function yandex() {
    if (file_exists(self::YANDEX_FILE)) {
      $xml = file_get_contents(self::YANDEX_FILE);
      $response = new Response();
      $response->setContent($xml);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    return ['content' => ['#markup' => 'Тут пока пусто =(']];
  }

  /**
   * Страница google.xml
   */
  public function google() {
    if (file_exists(self::GOOGLE_FILE)) {
      $xml = file_get_contents(self::GOOGLE_FILE);
      $response = new Response();
      $response->setContent($xml);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    return ['content' => ['#markup' => 'Тут пока пусто =(']];
  }

  /**
   * Страница vk_yandex.xml
   */
  public function vk_yandex() {
    if (file_exists(self::YANDEX_FILE)) {
      $xml = file_get_contents(self::YANDEX_FILE);
      $response = new Response();
      $response->setContent($xml);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    return ['content' => ['#markup' => 'Тут пока пусто =(']];
  }

  /**
   * Страница vk_google.xml
   */
  public function vk_google() {
    if (file_exists(self::GOOGLE_FILE)) {
      $xml = file_get_contents(self::GOOGLE_FILE);
      $response = new Response();
      $response->setContent($xml);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    return ['content' => ['#markup' => 'Тут пока пусто =(']];
  }

  /**
   * Страница vk_realty.xml
   */
  public function vk_realty() {
    if (file_exists(self::VK_REALTY_FILE)) {
      $xml = file_get_contents(self::VK_REALTY_FILE);
      $response = new Response();
      $response->setContent($xml);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    return ['content' => ['#markup' => 'Тут пока пусто =(']];
  }

  /**
   * Страница vk_transport.xml
   */
  public function vk_transport() {
    if (file_exists(self::VK_TRANSPORT_FILE)) {
      $xml = file_get_contents(self::VK_TRANSPORT_FILE);
      $response = new Response();
      $response->setContent($xml);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    return ['content' => ['#markup' => 'Тут пока пусто =(']];
  }

  /**
   * Страница vk_services.xml
   */
  public function vk_services() {
    if (file_exists(self::VK_SERVICES_FILE)) {
      $xml = file_get_contents(self::VK_SERVICES_FILE);
      $response = new Response();
      $response->setContent($xml);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    return ['content' => ['#markup' => 'Тут пока пусто =(']];
  }

  /**
   * Страница vk_hotel.xml
   */
  public function vk_hotel() {
    if (file_exists(self::VK_HOTEL_FILE)) {
      $xml = file_get_contents(self::VK_HOTEL_FILE);
      $response = new Response();
      $response->setContent($xml);
      $response->headers->set('Content-Type', 'text/xml');
      return $response;
    }
    return ['content' => ['#markup' => 'Тут пока пусто =(']];
  }

}

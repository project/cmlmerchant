<?php

namespace Drupal\cmlmerchant\Service;

/**
 * Interface \Drupal\cmlmerchant\Service\YmlServiceInterface.
 */
interface YmlServiceInterface {

  /**
   * Функция отладки генерации фида.
   */
  public function debug();

  /**
   * Сгенерировать фаил для яндекс фида с категориями и товарами.
   */
  public function yandex();

  /**
   * Сгенерировать фаил для яндекс фида с категориями и товарами.
   */
  public function google();

}

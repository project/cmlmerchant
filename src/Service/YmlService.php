<?php

namespace Drupal\cmlmerchant\Service;

use Drupal\commerce_product\Entity\Product;

/**
 * Class YmlService.
 */
class YmlService implements YmlServiceInterface {

  /**
   * SimpleXMLElement.
   *
   * @var \SimpleXMLElement
   */
  protected $xml;

  /**
   * Constructs a new YmlService object.
   */
  public function __construct() {
    $this->feed = '';
    $this->host = '';
    $this->xml = '';
    $this->shop = FALSE;
    $this->categories = [];
    $this->configUrl = FALSE;
    $this->config = \Drupal::config('cmlmerchant.settings');
    $this->getHost();
  }

  /**
   * {@inheritdoc}
   */
  public function createFeeds() {
    $this->yandex();
    $this->google();
    if ($this->config->get('vk_yandex')) {
      $this->vk_yandex();
    }
    if ($this->config->get('vk_google')) {
      $this->vk_google();
    }
    if ($this->config->get('vk_realty')) {
      $this->feed = 'vk_realty';
      $this->vk_realty();
    }
    if ($this->config->get('vk_transport')) {
      $this->feed = 'vk_transport';
      $this->vk_transport();
    }
    if ($this->config->get('vk_services')) {
      $this->feed = 'vk_services';
      $this->vk_services();
    }
    if ($this->config->get('vk_hotel')) {
      $this->feed = 'vk_hotel';
      $this->vk_hotel();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function yandex() {
    $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><yml_catalog/>');
    $this->prepareYandexXml();
    $this->prepareData();
    $this->fillDataToYandexXml();
  }

  /**
   * {@inheritdoc}
   */
  public function google() {
    $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><rss/>');
    $this->prepareGoogleXml();
    $this->prepareData();
    $this->fillDataToGoogleXml();
  }

  /**
   * {@inheritdoc}
   */
  public function vk_yandex() {
    $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><yml_catalog/>');
    $this->prepareYandexXml();
    $this->prepareData();
    $this->fillDataToYandexXml('/cmlmerchant_vk_yandex.xml');
  }

  /**
   * {@inheritdoc}
   */
  public function vk_google() {
    $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><rss/>');
    $this->prepareGoogleXml();
    $this->prepareData();
    $this->fillDataToGoogleXml('/cmlmerchant_vk_google.xml');
  }

  /**
   * {@inheritdoc}
   */
  public function vk_realty() {
    $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><realty-feed/>');
    $this->prepareVkRealtyXml();
    $this->getProductsWithoutCategories();
    $this->fillDataToVkRealtyXml();
  }

  /**
   * {@inheritdoc}
   */
  public function vk_transport() {
    $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><data/>');
    $this->getProductsWithoutCategories();
    $this->fillDataToVkTransportXml();
  }

  /**
   * {@inheritdoc}
   */
  public function vk_services() {
    $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><channel/>');
    $this->xml->addChild('title', $this->config->get('company_name'));
    $this->xml->addChild('link', $this->host);
    $this->getProductsWithoutCategories();
    $this->fillDataToVkServicesXml();
  }

  /**
   * {@inheritdoc}
   */
  public function vk_hotel() {
    $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><listings/>');
    $this->xml->addChild('title', $this->config->get('company_name'));
    $this->getProductsWithoutCategories();
    $this->fillDataToVkHotelXml();
  }

  /**
   * Заполнение yandex xml общими данными.
   */
  private function prepareYandexXml() {
    // Текущая дата.
    $date = \Drupal::service('date.formatter')->format(time(), 'custom', 'Y-m-d\TH:i:s');
    $this->xml->addAttribute('date', $date);
    // Информация о магазине.
    $this->shop = $this->xml->addChild('shop');
    $this->shop->addChild('name', $this->config->get('shop_name'));
    $this->shop->addChild('company', $this->config->get('company_name'));
    $this->shop->addChild('url', $this->host);
    $this->shop->addChild('platform', 'Drupal');
    // Информация об используемых валютах.
    $this->fillCurrencyInfo();
  }

  /**
   * Заполнение google xml общими данными.
   */
  private function prepareGoogleXml() {
    $ns = ['g' => 'http://base.google.com/ns/1.0'];
    foreach ($ns as $prefix => $name) {
      $this->xml->registerXPathNamespace($prefix, $name);
    }
    $ns = (object) $ns;
    $this->xml->addAttribute('version', '2.0');
    $this->xml->addAttribute('xmlns:xmlns:g', $ns->g);

    $this->shop = $this->xml->addChild('channel');
    $this->shop->addChild('title', $this->config->get('company_name'));
    $this->shop->addChild('link', $this->host);
  }

  /**
   * Заполнение Vk Realty xml общими данными.
   */
  private function prepareVkRealtyXml() {
    // Текущая дата.
    $date = \Drupal::service('date.formatter')->format(time(), 'custom', 'Y-m-d\TH:i:s');
    $this->xml->addChild('generation-date', $date);
  }

  /**
   * Заполнение xml данными о валютах.
   */
  private function fillCurrencyInfo() {
    $currencies = $this->shop->addChild('currencies');
    $rurCur = $currencies->addChild('currency');
    $rurCur->addAttribute('id', 'RUB');
    $rurCur->addAttribute('rate', '1');
  }

  /**
   * Подготовка данных для подстановки в фид.
   */
  private function prepareData() {
    $this->getCategories();
    $this->getProducts();
  }

  /**
   * Заполнение xml фида данными о товарах.
   */
  private function fillDataToYandexXml(string $file = '/cmlmerchant_yandex.xml') {
    $categories = $this->shop->addChild('categories');
    $offers = $this->shop->addChild('offers');
    foreach ($this->categories as $categoryId => $categoryInfo) {
      if (!empty($categoryInfo['products'])) {
        $this->setXmlCategory($categories, $categoryInfo);
        $this->setXmlProduct($offers, $categoryInfo);
      }
    }
    $str = $this->xml->asXML();
    $path = DRUPAL_ROOT . '/sites/default/files/YML';
    if (!file_exists($path)) {
      mkdir($path, 0775);
    }
    file_put_contents($path . $file, $str);
  }

  /**
   * Заполнение xml фида данными о товарах.
   */
  private function fillDataToGoogleXml(string $file = '/cmlmerchant_google.xml') {
    foreach ($this->categories as $categoryId => $categoryInfo) {
      // If (empty($categoryInfo['products']) || empty($categoryInfo['google_id'])) {
      //   continue;
      // }.
      foreach ($categoryInfo['products'] as $key => $productInfo) {
        foreach ($productInfo['variations'] as $variationInfo) {
          $item = $this->shop->addChild('item');
          $item->addChild("g:g:id", $productInfo['pid']);
          $item->addChild('title', $productInfo['title']);
          $item->addChild('g:g:description', $this->processDescription($productInfo['description']));
          $item->addChild('link', $productInfo['url']);
          $item->addChild('g:g:image_link', $productInfo['picture'][0]);
          $item->addChild('g:g:availability', 'in stock');
          $item->addChild('g:g:price', $variationInfo['price'] . ' RUB');
          // $item->addChild('g:g:google_product_category', $categoryInfo['google_id']);
        }
      }
    }
    $str = $this->xml->asXML();
    $path = DRUPAL_ROOT . '/sites/default/files/YML';
    if (!file_exists($path)) {
      mkdir($path, 0775);
    }
    file_put_contents($path . $file, $str);
  }

  /**
   * Заполнение xml фида данными о товарах.
   */
  private function fillDataToVkRealtyXml(string $file = '/cmlmerchant_vk_realty.xml') {
    foreach ($this->products as $productId => $productInfo) {
      if (!empty($productInfo)) {
        foreach ($productInfo['variations'] as $variationInfo) {
          if (!empty($productInfo['picture'])) {
            $offer = $this->xml->addChild('offer');
            $offer->addAttribute('internal-id', $productInfo['pid']);
            $offer->addChild('url', $productInfo['url']);
            $location = $offer->addChild('location');
            $location->addChild('country', $productInfo['country']);
            $location->addChild('region', $productInfo['region']);
            $location->addChild('locality-name', $productInfo['locality-name']);
            $location->addChild('address', $productInfo['address']);
            $price = $offer->addChild('price');
            $price->addChild('value', $variationInfo['price']);
            $price->addChild('currency', 'RUB');
            foreach ($productInfo['picture'] as $img) {
              $offer->addChild('image', $img);
            }
            if (!empty($productInfo['description'])) {
              $offer->addChild('description', $this->processDescription($productInfo['description']));
            }
          }
        }
      }
    }
    $str = $this->xml->asXML();
    $path = DRUPAL_ROOT . '/sites/default/files/YML';
    if (!file_exists($path)) {
      mkdir($path, 0775);
    }
    file_put_contents($path . $file, $str);
  }

  /**
   * Заполнение xml фида данными о товарах.
   */
  private function fillDataToVkTransportXml(string $file = '/cmlmerchant_vk_transport.xml') {
    $offers = $this->xml->addChild('cars');
    foreach ($this->products as $productId => $productInfo) {
      if (!empty($productInfo)) {
        foreach ($productInfo['variations'] as $variationInfo) {
          if (!empty($productInfo['picture'])) {
            $offer = $offers->addChild('car');
            $offer->addChild('unique_id', $productInfo['pid']);
            $offer->addChild('url', $productInfo['url']);
            $offer->addChild('mark_id', $productInfo['mark_id']);
            $offer->addChild('folder_id', $productInfo['folder_id']);
            $offer->addChild('owners_number', $productInfo['owners_number']);
            $offer->addChild('year', $productInfo['year']);
            $offer->addChild('price', $variationInfo['price']);
            $offer->addChild('currency', 'RUB');
            $offer->addChild('vin', $productInfo['vin']);
            $images = $offer->addChild('images');
            foreach ($productInfo['picture'] as $img) {
              $images->addChild('image', $img);
            }
            if (!empty($productInfo['description'])) {
              $offer->addChild('description', $this->processDescription($productInfo['description']));
            }
          }
        }
      }
    }
    $str = $this->xml->asXML();
    $path = DRUPAL_ROOT . '/sites/default/files/YML';
    if (!file_exists($path)) {
      mkdir($path, 0775);
    }
    file_put_contents($path . $file, $str);
  }

  /**
   * Заполнение xml фида данными о товарах.
   */
  private function fillDataToVkServicesXml(string $file = '/cmlmerchant_vk_services.xml') {
    $offers = $this->xml;
    foreach ($this->products as $productId => $productInfo) {
      if (!empty($productInfo)) {
        foreach ($productInfo['variations'] as $variationInfo) {
          if (!empty($productInfo['picture'])) {
            $offer = $offers->addChild('item');
            $offer->addChild('id', $productInfo['pid']);
            $offer->addChild('title', $productInfo['title']);
            $offer->addChild('link', $productInfo['url']);
            $offer->addChild('worker_type', $productInfo['worker_type']);
            $offer->addChild('brand', $productInfo['brand']);
            $offer->addChild('price', $variationInfo['price'] . ' RUB');
            $offer->addChild('image_link', $productInfo['picture'][0]);
            if (!empty($productInfo['description'])) {
              $offer->addChild('description', $this->processDescription($productInfo['description']));
            }
          }
        }
      }
    }
    $str = $this->xml->asXML();
    $path = DRUPAL_ROOT . '/sites/default/files/YML';
    if (!file_exists($path)) {
      mkdir($path, 0775);
    }
    file_put_contents($path . $file, $str);
  }

  /**
   * Заполнение xml фида данными о товарах.
   */
  private function fillDataToVkHotelXml(string $file = '/cmlmerchant_vk_hotel.xml') {
    $offers = $this->xml;
    foreach ($this->products as $productId => $productInfo) {
      if (!empty($productInfo)) {
        foreach ($productInfo['variations'] as $variationInfo) {
          if (!empty($productInfo['picture'])) {
            $offer = $offers->addChild('listing');
            $offer->addChild('hotel_id', $productInfo['pid']);
            $offer->addChild('name', $productInfo['title']);
            $offer->addChild('url', $productInfo['url']);
            $offer->addChild('price', $variationInfo['price'] . ' RUB');
            $location = $offer->addChild('location');
            $location->addChild('country', $productInfo['country']);
            $location->addChild('region', $productInfo['region']);
            $location->addChild('locality-name', $productInfo['locality-name']);
            $location->addChild('address', $productInfo['address']);
            $image = $offer->addChild('image');
            $image->addChild('url', $productInfo['picture'][0]);
            if (!empty($productInfo['description'])) {
              $offer->addChild('description', $this->processDescription($productInfo['description']));
            }
          }
        }
      }
    }
    $str = $this->xml->asXML();
    $path = DRUPAL_ROOT . '/sites/default/files/YML';
    if (!file_exists($path)) {
      mkdir($path, 0775);
    }
    file_put_contents($path . $file, $str);
  }

  /**
   * Запись категории в xml фид.
   */
  private function setXmlCategory(&$categories, $categoryInfo) {
    $termYml = $categories->addChild('category', $categoryInfo['name']);
    $termYml->addAttribute('id', $categoryInfo['id']);
    if (!empty($categoryInfo['parentId'])) {
      $termYml->addAttribute('parentId', $categoryInfo['parentId']);
    }
  }

  /**
   * Запись товара в xml фид.
   */
  private function setXmlProduct(&$offers, $categoryInfo) {
    foreach ($categoryInfo['products'] as $productInfo) {
      $this->setXmlProductVariation($offers, $productInfo);
    }
  }

  /**
   * Запись вариаций товара в xml фид.
   */
  private function setXmlProductVariation(&$offers, $productInfo) {
    foreach ($productInfo['variations'] as $variationInfo) {
      if (!empty($productInfo['picture'])) {
        $offer = $offers->addChild('offer');
        $offer->addAttribute('id', $productInfo['pid']);

        $offer->addChild('name', $productInfo['title']);
        $offer->addChild('url', $productInfo['url']);
        $offer->addChild('price', $variationInfo['price']);
        if (!empty($variationInfo['oldprice'])) {
          $offer->addChild('oldprice', $variationInfo['oldprice']);
        }
        $offer->addChild('currencyId', 'RUB');
        $offer->addChild('categoryId', $productInfo['categoryId']);
        if (!empty($productInfo['vendorCode'])) {
          $offer->addChild('vendorCode', $productInfo['vendorCode']);
        }
        foreach ($productInfo['picture'] as $img) {
          $offer->addChild('picture', $img);
        }
        // $offer->addChild('delivery', TRUE);
        if (!empty($productInfo['description'])) {
          $offer->addChild('description', $this->processDescription($productInfo['description']));
        }
        // $offer->addChild('sales_notes', 'Необходима предоплата.');
      }
    }
  }

  /**
   * Загрузка категорий.
   */
  private function getCategories($parent = 0, $child = FALSE) {
    $termsStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $terms = $termsStorage->loadTree('catalog', $parent, 1, TRUE);
    if (count($terms)) {
      foreach ($terms as $term) {
        $tid = $term->id();
        $category = [
          'name' => htmlspecialchars($term->getName()),
          'products' => [],
          'google_id' => $term->field_catalog_google_id->value,
          'id' => $tid,
        ];
        if ($child) {
          $category['parentId'] = $parent;
        }
        $this->categories[$tid] = $category;
        $this->getCategories($tid, TRUE);
      }
    }
  }

  /**
   * Загрузка товаров.
   */
  private function getProducts() {
    $categories = array_keys($this->categories);
    if (empty($categories)) {
      return;
    }
    $pids = \Drupal::entityQuery('commerce_product')->accessCheck(FALSE)
      ->condition('field_catalog', $categories, 'IN')
      ->condition('status', 1)
      ->execute();
    if (empty($pids)) {
      return;
    }
    $this->checkProducts($pids);
  }

  /**
   * Загрузка товаров без использования категорий.
   */
  private function getProductsWithoutCategories() {
    $pids = \Drupal::entityQuery('commerce_product')->accessCheck(FALSE)
      ->condition('status', 1)
      ->execute();
    if (empty($pids)) {
      return;
    }
    $this->checkProductsWithoutCategories($pids);
  }

  /**
   * Проверка товаров на наличие вариаций.
   */
  public function checkProducts($pids) {
    $products = Product::loadMultiple($pids);
    foreach ($products as $product) {
      if (!$product->hasVariations()) {
        continue;
      }
      $categoryId = $product->field_catalog->target_id;
      $item = [
        'pid' => $product->id(),
        'title' => $this->getProductTitle($product),
        'description' => $product->body->value ? htmlspecialchars($product->body->value) : '',
        'vendorCode' => $product->field_article->value ?? '',
        'categoryId' => $categoryId,
        'variations' => [],
        'picture' => $this->getProductPicture($product),
        'url' => $this->fixUrl($product->toUrl('canonical', ['absolute' => TRUE])->toString()),
      ];
      $this->checkVariations($item, $product->getVariations());
      if (!empty($item['variations'])) {
        $this->categories[$categoryId]['products'][$product->id()] = $item;
      }
    }
  }

  /**
   * Проверка товаров на наличие вариаций без использования категорий.
   */
  private function checkProductsWithoutCategories($pids) {
    $products = Product::loadMultiple($pids);
    foreach ($products as $product) {
      if (!$product->hasVariations()) {
        continue;
      }
      $categoryId = $product->field_catalog->target_id;
      $item = [
        'pid' => $product->id(),
        'title' => $this->getProductTitle($product),
        'description' => $product->body->value ? htmlspecialchars($product->body->value) : '',
        'vendorCode' => $product->field_article->value ?? '',
        'categoryId' => $categoryId ?? '',
        'variations' => [],
        'picture' => $this->getProductPicture($product),
        'url' => $this->fixUrl($product->toUrl('canonical', ['absolute' => TRUE])->toString()),
      ];
      $this->checkVariations($item, $product->getVariations());
      $this->checkOtherFields($item, $product);
      if (!empty($item['variations']) && !isset($item['error'])) {
        $this->products[$product->id()] = $item;
      }
    }
  }

  /**
   * Определение названия товара.
   */
  private function getProductTitle($product) {
    $siteProductnameField = $product->field_title;
    if (!$siteProductnameField->isEmpty()) {
      $productTitle = $siteProductnameField->value;
    }
    $title = empty($productTitle) ? $product->getTitle() : $productTitle;
    return htmlspecialchars($title);
  }

  /**
   * Заполнение товара информацией о вариации.
   */
  private function checkVariations(&$item, $variations) {
    foreach ($variations as $variation) {
      $price = (float) $variation->price->number;
      $stock = $variation->field_stock->value;
      if (!$variation->isActive() || $stock === 0 || !$price) {
        continue;
      }
      $variationInfo = [
        'price' => $price,
        'oldprice' => (float) $variation->field_oldprice->value > $price ? $variation->field_oldprice->value : '',
      ];
      $item['variations'][] = $variationInfo;
      break;
    }
  }

  /**
   * Дополнительные поля.
   */
  public function getOtherFields() {
    return [
      'vk_realty' => [
        'field_address' => [
          'bundle' => 'Адрес',
          'type' => 'textfield',
        ],
        'field_locality_name' => [
          'bundle' => 'Населенный пункт',
          'type' => 'textfield',
        ],
        'field_region' => [
          'bundle' => 'Регион',
          'type' => 'textfield',
        ],
        'field_country' => [
          'bundle' => 'Страна',
          'type' => 'textfield',
        ],
      ],
      'vk_transport' => [
        'field_mark' => [
          'bundle' => 'Марка или производитель транспортного средства',
          'type' => 'textfield',
        ],
        'field_folder' => [
          'bundle' => 'Модель транспортного средства',
          'type' => 'textfield',
        ],
        'field_owners_number' => [
          'bundle' => 'Текущее состояние транспортного средства (<a href="https://ads.vk.com/help/articles/ecomm_catalog_y_transport#owners_number" target="_blank">СМ. ОГРАНИЧЕНИЯ</a> <b>owner_numbers</b>)',
          'type' => 'list_string',
        ],
        'field_year' => [
          'bundle' => 'Год производства',
          'type' => 'textfield',
        ],
        'field_vin' => [
          'bundle' => 'VIN-номер',
          'type' => 'textfield',
        ],
      ],
      'vk_services' => [
        'field_tx_brand' => [
          'bundle' => 'Название бренда компании, которая предоставляет услугу',
          'type' => 'taxonomy',
        ],
        'field_worker_type' => [
          'bundle' => 'Тип исполнителя (<a href="https://ads.vk.com/help/articles/ecomm_catalog_service#worker_type" target="_blank">СМ. ОГРАНИЧЕНИЯ</a> <b>worker_type</b>)',
          'type' => 'list_string',
        ],
      ],
      'vk_hotel' => [
        'field_address' => [
          'bundle' => 'Адрес',
          'type' => 'textfield',
        ],
        'field_locality_name' => [
          'bundle' => 'Населенный пункт',
          'type' => 'textfield',
        ],
        'field_region' => [
          'bundle' => 'Регион',
          'type' => 'textfield',
        ],
        'field_country' => [
          'bundle' => 'Страна',
          'type' => 'textfield',
        ],
      ],
    ];
  }

  /**
   * Заполнение товара информацией о дополнительных полях.
   */
  private function checkOtherFields(&$item, $product) {
    $error = FALSE;
    switch ($this->feed) {
      case 'vk_realty':
        if (!$item['address'] = $product->field_address->value) {
          $error = TRUE;
        }
        if (!$item['locality-name'] = $product->field_locality_name->value) {
          $error = TRUE;
        }
        if (!$item['region'] = $product->field_region->value) {
          $error = TRUE;
        }
        if (!$item['country'] = $product->field_country->value) {
          $error = TRUE;
        }
        if ($error) {
          $item['error'] = TRUE;
        }
        break;

      case 'vk_transport':
        if (!$item['mark_id'] = $product->field_mark->value) {
          $error = TRUE;
        }
        if (!$item['folder_id'] = $product->field_folder->value) {
          $error = TRUE;
        }
        if (!$item['owners_number'] = $product->field_owners_number->value) {
          $error = TRUE;
        }
        if (!$item['year'] = $product->field_year->value) {
          $error = TRUE;
        }
        if (!$item['vin'] = $product->field_vin->value) {
          $error = TRUE;
        }
        if ($error) {
          $item['error'] = TRUE;
        }
        break;

      case 'vk_services':
        if ($brand = $product->field_tx_brand->entity) {
          $item['brand'] = $brand->getName();
        }
        else {
          $error = TRUE;
        }
        if (!$item['worker_type'] = $product->field_worker_type->value) {
          $error = TRUE;
        }
        if ($error) {
          $item['error'] = TRUE;
        }
        break;

      case 'vk_hotel':
        if (!$item['address'] = $product->field_address->value) {
          $error = TRUE;
        }
        if (!$item['locality-name'] = $product->field_locality_name->value) {
          $error = TRUE;
        }
        if (!$item['region'] = $product->field_region->value) {
          $error = TRUE;
        }
        if (!$item['country'] = $product->field_country->value) {
          $error = TRUE;
        }
        if ($error) {
          $item['error'] = TRUE;
        }
        break;

      default:
        break;
    }

  }

  /**
   * Получение изображения товара.
   */
  private function getProductPicture($product) {
    $images = [];
    if (is_object($image = $product->field_image->entity)) {
      $imageUri = $image->getFileUri();
      $url = file_create_url($imageUri);
      $url = $this->fixUrl($url);
      $images[] = $url;
    }
    if (!empty($product->field_gallery)) {
      foreach ($product->field_gallery as $image) {
        $imageUri = $image->entity->getFileUri();
        $url = file_create_url($imageUri);
        $url = $this->fixUrl($url);
        if (count($images) < 10) {
          $images[] = $url;
        }
      }
    }
    return $images;
  }

  /**
   * Получение изображения товара (было).
   */
  private function getProductPictureOld($product) {
    $field = $product->field_gallery;
    if (!$field->count()) {
      $field = $product->field_image;
      if ($field->isEmpty()) {
        return '';
      }
    }
    if (is_object($image = $field->entity)) {
      $imageUri = $image->getFileUri();
      // $url = ImageStyle::load('product_gallery_big')->buildUrl($imageUri);
      // $url = explode('?itok', $url)[0];
      $url = file_create_url($imageUri);
      $url = $this->fixUrl($url);
      return $url;
    }
    return '';
  }

  /**
   * Получение хоста.
   */
  private function getHost() {
    $this->configUrl = $this->config->get('site_url');
    $this->host = $this->fixUrl(\Drupal::request()->getSchemeAndHttpHost() . '/');
  }

  /**
   * В случаях, когда url начинается на http://default, заменяем его на url из конфига.
   */
  private function fixUrl($url) {
    if (!empty($this->configUrl)) {
      $url = str_replace('http://default', $this->configUrl, $url);
    }
    return $url;
  }

  /**
   * Оформляем описание.
   */
  private function processDescription($description) {
    return '<![CDATA[' . $description . ']]>';
  }

}

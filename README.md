# YML фид
Модуль предназначен для экспорта товаров в xml файл для интеграции с сервисом Яндекс.Маркет

# Структура файла
Структура файла соотвествует минимальным требованиям к YML-файлу.
Подробное описание всех элементов находится тут: https://yandex.ru/support/partnermarket/export/yml.html

## Описание структуры
* yml_catalog
    * shop
        * name
        * company
        * url
        * currencies
            * currency с атрибутом "RUR"
        * categories
            * category
        * offers
            * offer
                * name
                * url
                * price
                * currencyId со значением "RUR"
                * categoryId
                * picture
                * delivery со значением "1"
                * description
                * sales_notes со значением "Необходима предоплата."

# Обновление фида
Обновление происходит 1 раз в час. Этого интервала достаточно для обычного магазина.

# Пример структуры
`<yml_catalog date="2019-10-10T10:01:54">
    <shop>
        <name>Интернет-магазин бытовой техники</name>
        <company>ООО Интернет-магазин</company>
        <url>http://site.url/</url>
        <currencies>
            <currency id="RUR" rate="1"/>
        </currencies>
        <categories>
            <category id="22">Автомобильные видеорегистраторы</category>
        </categories>
        <offers>
            <offer id="77">
                <name>DVD-плеер BBK DVP033S</name>
                <url>
                    http://site.url/product/dvd-pleer-bbk-dvp033s
                </url>
                <price>2900</price>
                <currencyId>RUR</currencyId>
                <categoryId>22</categoryId>
                <picture>
                    http://site.url/sites/default/files/styles/product_gallery_big/public/product-image/2019-09/bbk_dvp033s.jpg
                </picture>
                <delivery>1</delivery>
                <description>
                    <![CDATA[<ul> <li>DVD-плеер</li> <li>воспроизведение с USB-накопителей</li> <li>поддержка MPEG4, DivX, XviD</li> <li>караоке</li> </ul> ]]>
                </description>
                <sales_notes>Необходима предоплата.</sales_notes>
            </offer>
        </offers>
    </shop>
</yml_catalog>`